<?php

namespace App\AxiomaBundle\Controller;

use App\AxiomaBundle\Entity\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AjaxController extends Controller
{
    /**
     * Get tags.
     *
     * @Route("/ajax/tags", name="ajax_get_tags", options={"expose"=true})
     * @Method("GET")
     */
    public function getTagsAction(Request $request)
    {
        if ($pattern = $request->get('term')) {
            $tags = $this->getDoctrine()->getRepository('AppAxiomaBundle:Tag')->getTagsByPattern($pattern);
        } elseif ($ids = $request->get('ids')) {
            $tags = $this->getDoctrine()->getRepository('AppAxiomaBundle:Tag')->getTagsByIds($ids);
        }

        $result = array();
        foreach ($tags as $item) {
            $result[] = array(
                'id' => $item->getId(),
                'text' => $item->getName(),
            );
        }

        return JsonResponse::create($result);
    }

    /**
     * Get tags.
     *
     * @Route("/ajax/tags", name="ajax_add_tag", options={"expose"=true})
     * @Method("PUT")
     */
    public function addTagAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $name = $request->get('name');

        if (!$tag = $this->getDoctrine()->getRepository('AppAxiomaBundle:Tag')->findOneBy(array('name' => $name))) {
            $tag = new Tag();
            $tag->setName($name);
            $entityManager->persist($tag);
            $entityManager->flush();
        }

        return JsonResponse::create($tag->getId());
    }
}
