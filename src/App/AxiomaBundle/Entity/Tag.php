<?php

namespace App\AxiomaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tag
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\AxiomaBundle\Entity\TagRepository")
 */
class Tag
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="Film")
     */
    private $film;

    /**
     * @ORM\ManyToMany(targetEntity="Book")
     */
    private $book;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->film = new \Doctrine\Common\Collections\ArrayCollection();
        $this->book = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add film
     *
     * @param \App\AxiomaBundle\Entity\Film $film
     * @return Tag
     */
    public function addFilm(\App\AxiomaBundle\Entity\Film $film)
    {
        $this->film[] = $film;
    
        return $this;
    }

    /**
     * Remove film
     *
     * @param \App\AxiomaBundle\Entity\Film $film
     */
    public function removeFilm(\App\AxiomaBundle\Entity\Film $film)
    {
        $this->film->removeElement($film);
    }

    /**
     * Get film
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFilm()
    {
        return $this->film;
    }

    /**
     * Add book
     *
     * @param \App\AxiomaBundle\Entity\Book $book
     * @return Tag
     */
    public function addBook(\App\AxiomaBundle\Entity\Book $book)
    {
        $this->book[] = $book;
    
        return $this;
    }

    /**
     * Remove book
     *
     * @param \App\AxiomaBundle\Entity\Book $book
     */
    public function removeBook(\App\AxiomaBundle\Entity\Book $book)
    {
        $this->book->removeElement($book);
    }

    /**
     * Get book
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBook()
    {
        return $this->book;
    }
}