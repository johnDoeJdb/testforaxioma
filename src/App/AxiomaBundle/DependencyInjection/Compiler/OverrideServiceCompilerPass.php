<?php

namespace App\AxiomaBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OverrideServiceCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('genemu.form.jquery.type.select2');
        $definition->setClass('App\AxiomaBundle\Form\Type\Select2Type');
    }
}