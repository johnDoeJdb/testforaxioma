<?php

namespace App\AxiomaBundle\Form;

use App\AxiomaBundle\Form\Transformer\TagTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BookType extends AbstractType
{
    private $entityManager;

    function __construct(ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('authors', 'genemu_jqueryselect2_entity', array(
                'class' => 'App\AxiomaBundle\Entity\Author',
                'multiple' => true,
            ))
            ->add('title')
            ->add('description')
            ->add('tags', 'genemu_jqueryselect2_tags', array(
                'transformer' => new TagTransformer($this->entityManager),
                'configs' => array(
                    'multiple' => true,
                )));
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'App\AxiomaBundle\Entity\Book',
            'translation_domain' => 'book',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'book';
    }
}
