<?php


namespace App\AxiomaBundle\Form\Transformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use App\AxiomaBundle\Entity\Tag;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;

class TagTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (tag) to a string (number).
     *
     * @param  tag|null $issue
     * @return string
     */
    public function transform($tags)
    {
        if (null === $tags) {
            return "";
        }
        $ids = array();
        if (is_object($tags) && get_class($tags) == 'Doctrine\ORM\PersistentCollection') {
            foreach ($tags->getValues() as $tag) {
                $ids[] = $tag->getId();
            }
        }

        $string = implode(',', $ids);

        return $string;
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $number
     *
     * @return tag|null
     *
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($ids)
    {
        $tags = new ArrayCollection();
        if (!$ids) {
            return $tags;
        } elseif (is_string($ids)) {
            $ids = explode(',', $ids);
        }

        foreach ($ids as $id) {
            $tag = $this->om
                ->getRepository('AppAxiomaBundle:Tag')
                ->find($id)
            ;
            if (!$tag) {
                throw new TransformationFailedException(sprintf(
                    'An tag with id "%s" does not exist!',
                    $tag
                ));
            }
            $tags->add($tag);
        }

        return $tags;
    }
}